import axios from 'axios';
import React, { Component } from 'react';

// UPDATE WITH YOUR API ENDPOINT
const baseURL ="https://29w17n0a04.execute-api.us-east-1.amazonaws.com/default/library";

class Books extends Component {

 constructor(props) {
    super(props);
    this.state = {value: ''};

    this.handleChange = this.handleChange.bind(this);
  }

  deleteBook(isbn, ev){
    // the isbn argument passed by deleteBook function on delete button MUST
    //TO PRECEDE the ev handler
    console.log('Book ISBN-->', isbn,' as javaScript Object-->', {params: {isbn:isbn}});
    
      // This axios syntax bellow is the only syntax that works. See the issue:
      
      // ## "When sending DELETE requests, Content-Type header is
      //     not present even though manually set."
      
      // --> https://github.com/axios/axios/issues/1083#issuecomment-449028890
      
      axios.delete(baseURL + "/books", { 
        headers: { "Content-Type": "application/json"}, 
        data: JSON.stringify({isbn:isbn})
      })
        .then(res => {
        //const items=res.data.Items;
        //console.log('Axios DELETE result-->', res);
        let cloneDeletedUsingES6 = [...this.props.items];
        cloneDeletedUsingES6 =  cloneDeletedUsingES6.filter(book=>{
          return book.isbn !== isbn;
        })
        
        console.log('Clone of the FILTERED STATE-->', cloneDeletedUsingES6);
        
        this.props.updateItems(cloneDeletedUsingES6);
        
        console.log('THIS ACTUAL STATE-->', this.props.items);
        
        });
    
    ev.preventDefault();
  
  }
  
  addBook(ev) {
    const inputISBN = ev.target.querySelector('[id="isbn"]');
    const isbn = inputISBN.value.trim();

    const inputTitle = ev.target.querySelector('[id="title"]');
    const title = inputTitle.value.trim();

    const inputPages = ev.target.querySelector('[id="pages"]');
    const pages = inputPages.value.trim();
    
    const selectAid = ev.target.querySelector('[id="author"]');
    const aid = selectAid.value.trim();


    console.log( "ISBN: " + isbn );
    console.log( "Title: " + title );
    console.log( "Pages: " + pages );
    console.log( "Aid: " + aid );
    
    
    let newbook = {
      isbn,
      title,
      pages,
      aid
    };
    
    axios.post( baseURL + "/books", newbook)
    .then(res => {
      //console.log('Axios POST response-->', res);
      const cloneUsingES6 = [...this.props.items];
      console.log('Clone of the ACTUAL STATE-->', cloneUsingES6);
      cloneUsingES6.push(newbook);
      this.props.updateItems(cloneUsingES6);
      inputTitle.value="";
      inputISBN.value="1";
      inputPages.value="100";
    });
    

    ev.preventDefault();
  }
  
  handleChange(event) {
    this.setState({value: event.target.value});
    console.log("selected value-->", event.target.value)
    
  }
  

  render() {

    const items = this.props.items;
    const authors = this.props.authors ? this.props.authors : [];
    let author;
    
    return(
    <div>
      <div className="py-5 text-center">
        <h2>Books</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addBook.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="isbn">ISBN</label>
                <input type="number" className="form-control" id="isbn" defaultValue="1" required />
                <div className="invalid-feedback">
                    An ISBN is required.
                </div>
              </div>
              <div className="col-md-10 mb-3">
                <label htmlFor="title">Title</label>
                <input type="text" className="form-control" id="title" defaultValue="" required />
                <div className="invalid-feedback">
                    A book title is required.
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-10 mb-3">
                <label htmlFor="author">Author</label>
                  <select className="form-control" id="author" required onChange={this.handleChange}>
                  {authors && authors.map(author => 
                    <option key={author.aid} value={author.aid}> {author.fname} {author.lname} </option>
                  )}
                  </select>
                <div className="invalid-feedback">
                    An author is required.
                </div>
              </div>
            
              <div className="col-md-2 mb-3">
                <label htmlFor="pages">Pages</label>
                <input type="number" className="form-control" id="pages" defaultValue="100" required />
                <div className="invalid-feedback">
                    The number of pages is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add book</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ISBN</th>
            <th scope="col">Title</th>
            <th scope="col">Pages</th>
            <th scope="col">Author</th>
          </tr>
        </thead>
        <tbody>

          {items && items.map(book => <tr key={book.isbn}>
            <th scope="row">{book.isbn}</th>
            <td>{book.title}</td>
            <td>{book.pages}</td>
            <td>{(author = authors.find(a=>a.aid === parseInt(book.aid))) && author.fname ? author.fname+" "+author.lname : "Not specified"}</td>
            <td><button onClick={this.deleteBook.bind(this, book.isbn)} type="button" className="btn btn-danger">Delete</button></td>
          </tr>)}

        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Books;
