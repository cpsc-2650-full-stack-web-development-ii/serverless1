import axios from 'axios';
import React, { Component } from 'react';

// UPDATE WITH YOUR API ENDPOINT
const baseURL ="https://29w17n0a04.execute-api.us-east-1.amazonaws.com/default/library";

class Authors extends Component {


  deleteAuthor(aid, ev){
    // the isbn argument passed by deleteBook function on delete button MUST
    //TO PRECEDE the ev handler
    console.log('Author aid-->', aid);
    
      // This axios syntax bellow is the only syntax that works. See the issue:
      
      // ## "When sending DELETE requests, Content-Type header is
      //     not present even though manually set."
      
      // --> https://github.com/axios/axios/issues/1083#issuecomment-449028890
      
      axios.delete(baseURL + "/authors", { 
        headers: { "Content-Type": "application/json" }, 
        data: JSON.stringify({aid:aid})
      })
        .then(res => {
        //const items=res.data.Items;
        //console.log('Axios DELETE result-->', res);
        let cloneDeletedUsingES6 = [...this.props.authors];
        cloneDeletedUsingES6 =  cloneDeletedUsingES6.filter(author=>{
          return author.aid !== aid;
        })
        
        console.log('DELETE ACTORS Clone of the FILTERED STATE-->', cloneDeletedUsingES6);
        
        this.props.updateAuthors(cloneDeletedUsingES6);
        
        console.log('DELETE ACTORS THIS ACTUAL STATE-->', this.props.authors);
        
        });// end of axios delete
    
    ev.preventDefault();
  
  }// end of deleteAuthor function
  
  addAuthor(ev) {
    const inputAid = ev.target.querySelector('[id="aid"]');
    const aid = inputAid.value.trim();

    const inputFname = ev.target.querySelector('[id="fname"]');
    const fname = inputFname.value.trim();

    const inputLname = ev.target.querySelector('[id="lname"]');
    const lname = inputLname.value.trim();

    console.log( "ID: " + aid );
    console.log( "First Name: " + fname );
    console.log( "Last Name: " + lname );
    
      let newauthor = {
        aid,
        fname,
        lname
      };
      
      axios.post( baseURL + "/authors", newauthor)
      .then(res => {
        //console.log('Axios POST response-->', res);
        let cloneAuthorUsingES6 = [...this.props.authors];
        console.log('Clone of the ACTUAL STATE-->', cloneAuthorUsingES6);
        cloneAuthorUsingES6.push(newauthor);
        this.props.updateAuthors(cloneAuthorUsingES6);
        inputAid.value=0;
        inputFname.value="";
        inputLname.value="";
      });
    

    ev.preventDefault();
  }
  


  render() {

    const authors = this.props.authors;
    
    return(
    <div>
      <div className="py-5 text-center">
        <h2>Authors</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addAuthor.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-2 mb-3">
                <label htmlFor="ID">ID</label>
                <input type="number" className="form-control" id="aid" defaultValue="0" required />
                <div className="invalid-feedback">
                    An ISBN is required.
                </div>
              </div>

              <div className="col-md-5 mb-3">
                <label htmlFor="fname">Fisrt Name</label>
                <input type="text" className="form-control" id="fname" defaultValue="" required />
                <div className="invalid-feedback">
                    A book title is required.
                </div>
              </div>

              <div className="col-md-5 mb-3">
                <label htmlFor="lname">Last Name</label>
                <input type="text" className="form-control" id="lname" defaultValue="" required />
                <div className="invalid-feedback">
                    The number of pages is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add author</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">First Name</th>
            <th scope="col">Last Name</th>
          </tr>
        </thead>
        <tbody>
          {authors && authors.map(author => <tr key={author.aid}>
            <th scope="row">{author.aid}</th>
            <td>{author.fname}</td>
            <td>{author.lname}</td>
            <td><button onClick={this.deleteAuthor.bind(this, author.aid)} type="button" className="btn btn-danger">Delete</button></td>
          </tr>)}
        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Authors;
