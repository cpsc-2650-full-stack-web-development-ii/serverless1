import axios from 'axios';
import React, { Component } from 'react';

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

import Books from './books';
import Authors from './authors';
import Home from './home';

const baseURL ="https://29w17n0a04.execute-api.us-east-1.amazonaws.com/default/library";

class Application extends Component {
  
     constructor (props) {
        super(props);
        this.state={
            authors:undefined,
            items:undefined
        };
        this.updateAuthors=this.updateAuthors.bind(this);
        this.updateItems=this.updateItems.bind(this);
     }
  
  componentDidMount(){

    axios.get( baseURL + "/books")
    .then(res => {
      const items=res.data.Items;
      console.log('componentDidMount--->Axios GET BOOKS result-->',items);
      this.setState({items:items});
    });
    

    axios.get( baseURL + "/authors")
    .then(res => {
      //console.log('all response --->', res);
      const authors=res.data.Items;
      console.log('componentDidMount--->Axios GET AUTHORS result-->\n',authors);
      this.setState({authors:authors});
    });
    
  }
  
  updateAuthors(authorsArr){
    this.setState({
      authors:authorsArr
    });
  }
  
  updateItems(itemsArr){
    this.setState({
      items:itemsArr
    });
  }
  

  render() {
    return (
    <Router>
      <div>
        <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
          <h5 className="my-0 mr-md-auto font-weight-normal"><Link className="p-2 text-dark" to="/">CPSC 2650 Library</Link></h5>
          <nav className="my-2 my-md-0 mr-md-3">
            <Link className="p-2 text-dark" to="/books">Books</Link>
            <Link className="p-2 text-dark" to="/authors">Authors</Link>
          </nav>
        </div>

        {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
        <Switch>
          <Route path="/authors">
            <Authors authors={this.state.authors} updateAuthors={this.updateAuthors} />
          </Route>
          <Route path="/books">
            <Books items={this.state.items} updateItems={this.updateItems} authors={this.state.authors} />
          </Route>
          <Route path="/">
            <Home />
          </Route>
        </Switch>
      </div>
    </Router>
  );
  }
}

export default Application;
