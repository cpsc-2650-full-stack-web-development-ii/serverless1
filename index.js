//AWS Lambda function contents

var AWS = require("aws-sdk");

AWS.config.update({
  region: "us-east-1"
});

var docClient = new AWS.DynamoDB.DocumentClient();

var table1 = "books";
var table2 = "authors";


const corsHeaders = {
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': 'content-type'

    };

async function newBook(book) {
    book.isbn = parseInt(book.isbn);
    book.pages = parseInt(book.pages);
    book.aid = parseInt(book.aid);
    
    let params = {
        TableName: table1,
        Item: book
    };
    
    const awsRequest = docClient.put(params);
    return awsRequest.promise()
    .then(data => {
        console.log("Yay! I'm inside PUT --> newBook function <---");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        console.log("Uh-oh! Error inside PUT newBook function --->");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}


async function newAuthor(author) {
    author.aid = parseInt(author.aid);
    
    let params = {
        TableName: table2,
        Item: author
    };
    
    const awsRequest = docClient.put(params);
    return awsRequest.promise()
    .then(data => {
        console.log("Yay!I'm inside PUT --> newAuthor function <---");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        console.log("Uh-oh!Error inside PUT newAuthor function --->");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}



async function deleteBook(book) {
    book.isbn=parseInt(book.isbn);
    let params = {
        TableName: table1,
        Key:{
            isbn:book.isbn
        }
    };
    
    const awsRequest = docClient.delete(params);
    return awsRequest.promise()
    .then(data => {
        console.log("Yay!I'm inside DELETE --> deleteBooks function <---");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        console.log("Uh-oh!  Error inside DELETE deleteBooks function --->");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
    

}

////
async function deleteAuthor(author) {
    author.aid = parseInt(author.aid);
    let params = {
        TableName: table2,
        Key:{
            aid:author.aid
        }
    };
    
    const awsRequest = docClient.delete(params);
    return awsRequest.promise()
    .then(data => {
        console.log("Yay!I'm inside DELETE --> deleteAuthor function <---");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        console.log("Uh-oh!  Error inside DELETE deleteAuthor function --->");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
    

}
///

async function allBooks() {
    
    let params = {
        TableName: table1
    };
    
    const awsRequest = docClient.scan(params);
    return awsRequest.promise()
    .then(data => {
        console.log("Yay! I'm inside GET --> allBooks function <---");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        console.log("Uh-oh! Error inside GET allBooks function --->");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}


async function allAuthors() {
    
    let params = {
        TableName: table2
    };
    
    const awsRequest = docClient.scan(params);
    return awsRequest.promise()
    .then(data => {
        console.log("Yay! I'm inside GET--> allAuthors function <---");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(data)
        };
        return response;
    },err => {
        console.log("Uh-oh! Error inside GET allAuthors function --->");
        const response = {
            statusCode: 200,
            headers: corsHeaders,
            body: JSON.stringify(err)
        };
        return response;
    });
}



exports.handler = async (event) => {
    
   
  console.log(event);
    // sample event properties:
    // event.path: '/default/library/books'
    // event.httpMethod: 'POST'
    // event.body: json encoded object

    console.log( event.path );
    console.log( event.httpMethod );
    let body = JSON.parse(event.body);
    console.log( body );
    
    if ( event.httpMethod == 'POST') {
        if ( event.path == '/default/library/books') {
            return newBook(body);
        } else if ( event.path == '/default/library/authors') {
            return newAuthor(body);
        }
    }
    
    if ( event.httpMethod == 'GET') {
        if ( event.path == '/default/library/books') {
            return allBooks();
        } else if ( event.path == '/default/library/authors') {
            return allAuthors();
        }
    }
    
    if ( event.httpMethod == 'DELETE'){
        if ( event.path == '/default/library/books') {
            return deleteBook(body);
        } else if ( event.path == '/default/library/authors'){
            return deleteAuthor(body);
        }
    }


    const response = {
        statusCode: 200,
        headers: corsHeaders,
        body: JSON.stringify('Unrecognized request'),
    };
    return response;
};