In order to fix CORS errors browser messages in AXIOS POST and DELETE methods
it is necessary to make the following changes on section CORS in AWS HTTP API
Gateway:

(1) Access-Control-Allow-Origin set up with * value;

(2) Access-Control-Allow-Headers set up with * value;

(3) Access-Control-Allow-Methods set up with * value;

(4) Leave all other fields with their default value;